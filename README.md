In this R file you can have a look at training and testing a K top scoring pair classifier thanks to switchbox library. We used genes expressions data about breast cancer from this library (78 patients and 79 genes). We followed a tutorial from bioconductor : https://www.bioconductor.org/packages/devel/bioc/vignettes/switchBox/inst/doc/switchBox.pdf

The switchbox library allows to :

- 1. Filter the features to be used to develop the classifier (i.e., differentially expressed genes);

- 2. Compute the scores for all available feature pairs to identify the top performing TSPs;

- 3. Compute the scores for selected feature pairs to identify the top performing TSPs;

- 4. Identify the number of top pairs, K, to be used in the final classifier;

- 5. Compute individual TSP votes for one class or the other and aggregate the votes based on various methods;

- 6. Classify new samples based on the top KTSP based on various methods;


Author : Marion Estoup 

Mail : marion_110@hotmail.fr

Date : June 2023

